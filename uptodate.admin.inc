<?php

/**
 * Form builder; The general Up to Date settings form.
 *
 * @ingroup forms
 * @see system_settings_form()
 * @param $form
 * @return
 */
function uptodate_admin_settings($form) {
  global $user;
  global $base_url;

  $form['uptodate_site_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Site name'),
    '#description' => t('The name of the site as it will appear in Up to Date.'),
    '#default_value' => variable_get('uptodate_site_name', variable_get('site_name', 'Drupal')),
    '#required' => TRUE,
  );
  $form['uptodate_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t("Your Up to Date username."),
    '#default_value' => variable_get('uptodate_username', $user->mail),
    '#required' => TRUE,
  );
  $form['uptodate_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t("Your Up to Date password."),
    '#default_value' => variable_get('uptodate_password', ''),
    '#required' => TRUE,
  );
  $form['uptodate_base_url'] = array(
    '#type' => 'value',
    '#value' => $base_url,
  );

  $form['#validate'][] = 'uptodate_admin_settings_validate';

  return system_settings_form($form);
}

/**
 * Check that the username and password are valid.
 *
 * @param $form
 * @param $form_state
 */
function uptodate_admin_settings_validate($form, &$form_state) {
  $valid = TRUE;
  $username = trim($form_state['values']['uptodate_username']);
  $password = $form_state['values']['uptodate_password'];
  $site_name = trim($form_state['values']['uptodate_site_name']);

  $result = uptodate_ping($username, $password);
  if (is_array($result)) {
    form_set_error('uptodate_username', t('The username/password/site name combination is not valid.'));
  }
  else {
    drupal_set_message(t('Your credentials validated successfully.'));
  }

  // Save trimmed fields.
  $form_state['values']['uptodate_site_name'] = $site_name;
  $form_state['values']['uptodate_username'] = $username;
}
