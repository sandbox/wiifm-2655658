Up to Date
----------

This module integrates your Drupal site with the product upto.date. Every day
you site will send it's current module and core version information over.

Requirements
------------

* Drupal 7
* Up to Date username and password from upto.date

Installation and configuration
------------------------------

1. Generate a username and password for the upto.date site. You will also need
   to generate a new site to report to. This information will be used later when
   you configure the Drupal module.

2. Install the Up to Date module as you install a contributed Drupal module.
   See https://drupal.org/documentation/install/modules/themes/modules-7

3. Go to /admin/config/services/uptodate to configure the module.

   Here you will need to paste in the username, password and site name.

Further reading
---------------

[link to public docs]
